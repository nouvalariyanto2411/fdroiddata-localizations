#!/bin/bash

# Fix TypographyEllipsis programmatically

sed -i 's/\.\.\./…/g' localizations/*.xliff
if git diff | grep -Eo '^\+.*…'; then
    echo Fix TypographyEllipsis
    exit 1
fi
